import sys
import json
# app.
from flask import Flask           # import flask
app = Flask(__name__)             # create an app instance

@app.route("/")                   # at the end point /
def hello():# call method hello
    d = {"a":1, "b":99}
    return json.dumps(d)          # which returns "hello world"if __name__ == "__main__":        # on running python app.py
    
if __name__ == "__main__":        # on running python app.py
    print(sys.argv)
    app.run(host='0.0.0.0', port=int(sys.argv[1]))    
